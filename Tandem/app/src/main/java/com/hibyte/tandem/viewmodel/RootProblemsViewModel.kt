package com.hibyte.tandem.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hibyte.tandem.model.ProblemNode
import com.hibyte.tandem.repository.ProblemNodeRepository
import com.hibyte.tandem.ui.items.ProblemItem
import kotlinx.coroutines.launch

class RootProblemsViewModel @ViewModelInject constructor(
    private val _problemNodeRepository: ProblemNodeRepository
) : ViewModel() {

    val rootProblemNodeItems =
        Transformations.map(_problemNodeRepository.getRootProblemNodes()) { problemNodes ->
            problemNodes.map { problemNode ->
                ProblemItem(
                    problemNode.id,
                    problemNode.nodeText
//                problemNode.duration?.toString()
                )
            }
        }

    fun populateDatabase() {
        viewModelScope.launch {
            _problemNodeRepository.sync()
        }
    }
}