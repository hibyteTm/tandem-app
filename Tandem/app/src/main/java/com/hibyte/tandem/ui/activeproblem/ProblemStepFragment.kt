package com.hibyte.tandem.ui.activeproblem

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.hibyte.tandem.R
import com.hibyte.tandem.ui.items.ProblemItem
import com.hibyte.tandem.viewmodel.ActiveProblemViewModel
import kotlinx.android.synthetic.main.fragment_problem_step.*

class ProblemStepFragment : Fragment(R.layout.fragment_problem_step) {
    private val _activeProblemViewModel: ActiveProblemViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )
    private lateinit var _listAdapter: NextStepProblemListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        subscribeActiveProblemViewModel()
    }

    private fun initRecyclerView() {
        _listAdapter = NextStepProblemListAdapter(R.layout.list_item_next_step_problem)

        next_step_recycler_view.adapter = _listAdapter

        _listAdapter.setOnItemClickListener { v, i ->
            onClickItem(v, i)
        }
    }

    private fun subscribeActiveProblemViewModel() {
        _activeProblemViewModel.activeProblemItem.observe(viewLifecycleOwner) {
//            findNavController().navigate(R.id.action_problemStepFragment_to_activeProblemFragment)
        }
        _activeProblemViewModel.nextStepProblemItems.observe(viewLifecycleOwner) { list ->
            if (list.isEmpty()) {
                _activeProblemViewModel.finishProblem()
                findNavController().popBackStack()
            } else
                _listAdapter.set(list)
        }
    }

    private fun onClickItem(view: View, item: ProblemItem) {
        _activeProblemViewModel.activateProblemNode(item.problemNodeId)
    }
}