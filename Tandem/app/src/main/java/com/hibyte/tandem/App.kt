package com.hibyte.tandem

import android.app.Application
import com.hibyte.tandem.utils.GlobalConst
import dagger.hilt.android.HiltAndroidApp
import one.space.networking.core.SpoClient
import one.space.networking.core.SpoClientConfig
import one.space.networking.core.model.SpoAppCredentials

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        //Space One
        SpoClient.createDefault(
            spoClientConfig = SpoClientConfig(
                context = this,
                baseUrl = GlobalConst.BASE_URL,
                scope = GlobalConst.SCOPE_KEY,
                appName = GlobalConst.APP_NAME,
                appVersion = GlobalConst.APP_VERSION,
                appCredentials = SpoAppCredentials.fromOAuthClientSecret(
                    clientId = GlobalConst.CLIENT_ID,
                    clientSecret = GlobalConst.CLIENT_SECRET
                )
            )
        )
    }
}