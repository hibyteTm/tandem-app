package com.hibyte.tandem.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.Expose
import one.space.networking.core.annotations.SpoEntity
import one.space.networking.core.annotations.SpoItemId
import java.util.*

@SpoEntity("node")
@Entity(tableName = "problemNodes")
@TypeConverters(DataConverter::class)
data class ProblemNode(

    @SpoItemId
    @PrimaryKey(autoGenerate = true)
    @Expose(serialize = false, deserialize = true)
    var id: Long = 0L,

    @ColumnInfo(name = "nodeText")
    var nodeText: String = "",

    @ColumnInfo(name = "duration")
    var duration: Double? = null,

    @ColumnInfo(name = "assets")
    var assets: List<Long>? = null,

    @ColumnInfo(name = "children")
    var children: List<Long>? = null,

    @ColumnInfo(name = "isFinal")
    var isFinal: Boolean = false,

    @ColumnInfo(name = "isExecuted")
    var isExecuted: Boolean = false,

    @ColumnInfo(name = "userCode")
    var userCode: String? = null,

    @ColumnInfo(name = "isActive")
    var isActive: Boolean = false,

    @ColumnInfo(name = "isDone")
    var isDone: Boolean = false,

    @ColumnInfo(name = "description")
    var description: String? = null,

    @ColumnInfo(name = "missionTime")
    var missionTime: Date? = null
)