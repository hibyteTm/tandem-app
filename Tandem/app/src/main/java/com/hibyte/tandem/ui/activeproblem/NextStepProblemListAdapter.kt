package com.hibyte.tandem.ui.activeproblem

import android.view.View
import androidx.annotation.LayoutRes
import com.hibyte.tandem.ui.items.ProblemItem
import com.hibyte.tandem.utils.BaseAdapter
import kotlinx.android.synthetic.main.list_item_next_step_problem.view.*

import kotlinx.android.synthetic.main.list_item_problem.view.problem_name_text_view

class NextStepProblemListAdapter(@LayoutRes layoutId: Int) : BaseAdapter<ProblemItem>(layoutId) {
    override fun createViewHolder(view: View) = NextStepProblemViewHolder(view)
}

class NextStepProblemViewHolder(private val _view: View) :
    BaseAdapter.ViewHolder<ProblemItem>(_view) {
    override fun hold(
        item: ProblemItem,
        onClickListener: BaseAdapter.OnItemClickListener<ProblemItem>?
    ) {
        _view.problem_name_text_view.text = item.name
        _view.problem_duration_text_view.text = item.duration
        _view.recommended_text_view.visibility = if (item.isRecommended) View.VISIBLE else View.GONE
        _view.setOnClickListener {
            onClickListener?.onItemClicked(it, item)
        }
    }

}