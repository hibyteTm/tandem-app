package com.hibyte.tandem.dao

import androidx.room.*
import com.google.android.material.circularreveal.CircularRevealHelper
import com.hibyte.tandem.model.UserCode

@Dao
interface UserCodeDAO {

    @Query("SELECT * FROM userCodes LIMIT 1")
    suspend fun getFirst(): UserCode

    @Query("SELECT COUNT(id) FROM userCodes")
    suspend fun getCount(): Int

    @Update
    suspend fun update(code: UserCode)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(code: UserCode)
}