package com.hibyte.tandem.ui.activeproblem

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.hibyte.tandem.R
import kotlinx.android.synthetic.main.fragment_active_problem.*


class ActiveProblemFragment : Fragment(R.layout.fragment_active_problem) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        next_button.setOnClickListener {
            findNavController().navigate(R.id.action_activeProblemFragment_to_problemReportFragment)
        }
    }
}