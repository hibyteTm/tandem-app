package com.hibyte.tandem.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hibyte.tandem.repository.ProblemNodeRepository
import com.hibyte.tandem.ui.items.ProblemItem
import com.hibyte.tandem.utils.toMinutesString
import kotlinx.coroutines.launch

class ActiveProblemViewModel @ViewModelInject constructor(
    private val _problemNodeRepository: ProblemNodeRepository
) : ViewModel() {

    private val _activeProblemItem = MutableLiveData<ProblemItem>()
    val activeProblemItem: LiveData<ProblemItem>
        get() = _activeProblemItem

    private val _nextStepProblemItems = MutableLiveData<List<ProblemItem>>()
    val nextStepProblemItems: LiveData<List<ProblemItem>>
        get() = _nextStepProblemItems

    private val _onAddDescription = MutableLiveData(false)
    val onAddDescription: LiveData<Boolean>
        get() = _onAddDescription

    fun activateProblemNode(id: Long) {
        viewModelScope.launch {
            val problemNode = _problemNodeRepository.getProblemNode(id)
            _activeProblemItem.value = ProblemItem(
                problemNode.id,
                problemNode.nodeText,
                problemNode.duration?.toMinutesString()
            )

            problemNode.children?.let { list ->
                val nextStepNodes = _problemNodeRepository.getProblemNodes(list)
                _nextStepProblemItems.value = nextStepNodes.map {
                    ProblemItem(
                        it.id,
                        it.nodeText,
                        it.duration?.toMinutesString()
                    )
                }
            }

            _problemNodeRepository.addDescription(problemNode.id, "d")
//            _problemNodeRepository.startProblem(id)
        }
    }

    fun addDescription(description: String) {
        _activeProblemItem.value?.problemNodeId?.let {
            viewModelScope.launch {
                _problemNodeRepository.addDescription(it, description)
                _onAddDescription.value = true
            }
        }
    }

    fun descriptionAdded() {
        _onAddDescription.value = false
    }

    fun finishProblem() {
        _activeProblemItem.value = null
    }

}