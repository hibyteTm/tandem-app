package com.hibyte.tandem.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class DataConverter {

    @TypeConverter
    fun fromCountryLangList(value: List<Long>?): String? {
        val gson = Gson()
        val type = object : TypeToken<List<Long>?>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toCountryLangList(value: String?): List<Long>? {
        val gson = Gson()
        val type = object : TypeToken<List<Long>?>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromDate(value: Date?): String? {
        val gson = Gson()
        val type = object : TypeToken<Date?>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toDate(value: String?): Date? {
        val gson = Gson()
        val type = object : TypeToken<Date?>() {}.type
        return gson.fromJson(value, type)
    }
}