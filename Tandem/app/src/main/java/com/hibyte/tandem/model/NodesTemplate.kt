package com.hibyte.tandem.model

data class NodesTemplate(
    val nodes: List<ProblemNode>
)