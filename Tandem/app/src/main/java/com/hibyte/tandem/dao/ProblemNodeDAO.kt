package com.hibyte.tandem.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.hibyte.tandem.model.ProblemNode

@Dao
interface ProblemNodeDAO {

    @Query("SELECT * FROM problemNodes WHERE id=:id")
    suspend fun getProblemNode(id: Long): ProblemNode

    @Query("SELECT * FROM problemNodes WHERE id IN (:ids)")
    suspend fun getProblemNodes(ids: List<Long>): List<ProblemNode>

    @Query("SELECT COUNT(id) FROM problemNodes")
    suspend fun getCount(): Int

    @Query("SELECT * FROM problemNodes")
    fun getAll(): LiveData<List<ProblemNode>>

    @Query("SELECT * FROM problemNodes WHERE userCode NOT NULL")
    fun getRootProblemNodes(): LiveData<List<ProblemNode>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(problemNode: ProblemNode)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(problemNodes: List<ProblemNode>)

    @Update
    suspend fun update(problemNode: ProblemNode)

    @Query("DELETE FROM problemNodes")
    suspend fun deleteAll()
}