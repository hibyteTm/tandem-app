package com.hibyte.tandem.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hibyte.tandem.repository.UserCodeRepository
import kotlinx.coroutines.launch

class UserCodeViewModel @ViewModelInject constructor(
    private val _userCodeRepository: UserCodeRepository
) : ViewModel() {

    private val _userCode = MutableLiveData<String>()
    val userCode: LiveData<String>
        get() = _userCode

    private val _onSubmit = MutableLiveData(false)
    val onSubmit: LiveData<Boolean>
        get() = _onSubmit

    fun loadUserCode() {
        viewModelScope.launch {
            _userCode.value = _userCodeRepository.getUserCode().code
        }
    }

    fun submitUserCode(code: String) {
        viewModelScope.launch {
            _userCodeRepository.updateUserCode(code)
            _onSubmit.value = true
        }
    }
}