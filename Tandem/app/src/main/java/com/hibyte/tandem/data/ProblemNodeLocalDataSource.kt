package com.hibyte.tandem.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hibyte.tandem.dao.ProblemNodeDAO
import com.hibyte.tandem.dao.UserCodeDAO
import com.hibyte.tandem.model.ProblemNode
import com.hibyte.tandem.model.UserCode

@Database(
    entities = [ProblemNode::class, UserCode::class], version = 3, exportSchema = false
)
abstract class ProblemNodeLocalDataBase : RoomDatabase() {
    abstract fun getProblemNodeDao(): ProblemNodeDAO

    abstract fun getUserCodeDao(): UserCodeDAO
}