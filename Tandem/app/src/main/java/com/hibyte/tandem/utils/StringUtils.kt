package com.hibyte.tandem.utils

fun Double.toMinutesString() = String.format("%.0fm", this)