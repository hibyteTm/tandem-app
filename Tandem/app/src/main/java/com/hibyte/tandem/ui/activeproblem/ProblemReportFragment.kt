package com.hibyte.tandem.ui.activeproblem

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.hibyte.tandem.R
import com.hibyte.tandem.viewmodel.ActiveProblemViewModel
import kotlinx.android.synthetic.main.fragment_problem_report.*


class ProblemReportFragment : Fragment(R.layout.fragment_problem_report) {

    private val _activeProblemViewModel: ActiveProblemViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeActiveProblemViewModel()
        submit_button.setOnClickListener {
            _activeProblemViewModel.addDescription(problem_description_edit_text.text.toString())
        }
    }

    private fun subscribeActiveProblemViewModel() {
        _activeProblemViewModel.activeProblemItem.observe(viewLifecycleOwner) { problemItem ->
            problem_name_text_view.text = problemItem.name
        }
        _activeProblemViewModel.onAddDescription.observe(viewLifecycleOwner) {
            if (it) {
                _activeProblemViewModel.descriptionAdded()
                findNavController().navigate(R.id.action_problemReportFragment_to_problemStepFragment)
            }
        }
    }
}