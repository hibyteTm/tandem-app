package com.hibyte.tandem.repository

import android.net.Uri
import com.google.gson.reflect.TypeToken
import com.hibyte.tandem.dao.ProblemNodeDAO
import com.hibyte.tandem.dao.UserCodeDAO
import com.hibyte.tandem.model.ProblemNode
import com.hibyte.tandem.utils.GlobalConst
import one.space.networking.core.SpoClient
import java.util.*
import javax.inject.Inject

class ProblemNodeRepository @Inject constructor(
    private val _problemNodeDAO: ProblemNodeDAO,
    private val _userCodeDAO: UserCodeDAO,
    private val _spoClient: SpoClient
) {

    suspend fun getCount() = _problemNodeDAO.getCount()

    fun getAll() = _problemNodeDAO.getAll()

    suspend fun getProblemNode(id: Long) = _problemNodeDAO.getProblemNode(id)

    suspend fun getProblemNodes(ids: List<Long>) = _problemNodeDAO.getProblemNodes(ids)

    fun getRootProblemNodes() = _problemNodeDAO.getRootProblemNodes()

    suspend fun insert(problemNode: ProblemNode) = _problemNodeDAO.insert(problemNode)

    suspend fun insertAll(problemNodes: List<ProblemNode>) = _problemNodeDAO.insertAll(problemNodes)

    suspend fun deleteAll() = _problemNodeDAO.deleteAll()

    private suspend fun update(problemNode: ProblemNode) {
        _problemNodeDAO.update(problemNode)
//        _spoClient.item().updateItem(problemNode)
        _spoClient.custom()
            .get<Void>(
                Uri.parse("/api/scope/${GlobalConst.SCOPE_KEY}/spaceapp/${problemNode.id}/${problemNode.description}"),
                Void::class.java
            )
    }

    suspend fun addDescription(id: Long, description: String) {
        val problemNode = _problemNodeDAO.getProblemNode(id)
        problemNode.isActive = true
        problemNode.missionTime = Date()
        problemNode.description = description
        update(problemNode)
    }

    suspend fun startProblem(id: Long) {
        val problemNode = _problemNodeDAO.getProblemNode(id)
        problemNode.isActive = true
        problemNode.missionTime = Date()
        update(problemNode)
    }

    suspend fun executeProblem(id: Long) {
        val problemNode = _problemNodeDAO.getProblemNode(id)
        problemNode.isExecuted = true
        update(problemNode)
    }

    suspend fun sync() {
        deleteAll()
        val userCode = _userCodeDAO.getFirst().code
        val response = _spoClient.custom()
            .get<List<ProblemNode>>(
                Uri.parse("/api/scope/${GlobalConst.SCOPE_KEY}/spaceapp/$userCode"),
                object : TypeToken<List<ProblemNode>>() {}.type
            )
        response.content?.let { insertAll(it) }
    }
}