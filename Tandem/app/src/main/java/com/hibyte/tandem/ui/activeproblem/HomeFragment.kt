package com.hibyte.tandem.ui.activeproblem

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.hibyte.tandem.R
import com.hibyte.tandem.ui.items.ProblemItem
import com.hibyte.tandem.viewmodel.ActiveProblemViewModel
import com.hibyte.tandem.viewmodel.RootProblemsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home) {

    private val _rootProblemsViewModel: RootProblemsViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )
    private val _activeProblemViewModel: ActiveProblemViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )
    private lateinit var _listAdapter: ProblemListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        subscribeRootProblemsViewModel()
        subscribeActiveProblemViewModel()
    }

    override fun onResume() {
        super.onResume()
        _rootProblemsViewModel.populateDatabase()
    }

    private fun initRecyclerView() {
        _listAdapter = ProblemListAdapter(R.layout.list_item_problem)

        home_recycler_view.adapter = _listAdapter

        _listAdapter.setOnItemClickListener { v, i ->
            onClickItem(v, i)
        }
    }

    private fun subscribeRootProblemsViewModel() {
        _rootProblemsViewModel.rootProblemNodeItems.observe(viewLifecycleOwner) {
            _listAdapter.set(it)
        }
    }

    private fun subscribeActiveProblemViewModel() {
        _activeProblemViewModel.activeProblemItem.observe(viewLifecycleOwner) { item ->
            item?.let {
                findNavController().navigate(R.id.action_homeFragment_to_problemReportFragment)
            }
        }
    }

    private fun onClickItem(view: View, item: ProblemItem) {
        _activeProblemViewModel.activateProblemNode(item.problemNodeId)
    }
}