package com.hibyte.tandem.ui.usercode

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.hibyte.tandem.R
import com.hibyte.tandem.viewmodel.UserCodeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_user_code.*

@AndroidEntryPoint
class UserCodeFragment : Fragment(R.layout.fragment_user_code) {

    private val _userCodeViewModel: UserCodeViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeUserCodeViewModel()
        submit_button.setOnClickListener {
            _userCodeViewModel.submitUserCode(user_code_edit_text.text.toString())
        }
        _userCodeViewModel.loadUserCode()
    }

    private fun subscribeUserCodeViewModel() {
        _userCodeViewModel.userCode.observe(viewLifecycleOwner) {
            user_code_edit_text.setText(it)
        }
        _userCodeViewModel.onSubmit.observe(viewLifecycleOwner) {
            if (it) findNavController().navigate(R.id.action_userCodeFragment_to_homeFragment)
        }
    }
}