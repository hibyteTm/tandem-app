package com.hibyte.tandem.ui.items

data class ProblemItem(
    val problemNodeId: Long,
    val name: String,
    val duration: String? = null,
    val isRecommended: Boolean = false
)