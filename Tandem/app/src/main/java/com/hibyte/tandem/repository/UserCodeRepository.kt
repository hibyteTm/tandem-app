package com.hibyte.tandem.repository

import com.hibyte.tandem.dao.UserCodeDAO
import com.hibyte.tandem.model.UserCode
import javax.inject.Inject

class UserCodeRepository @Inject constructor(
    private val _userCodeDAO: UserCodeDAO
) {
    suspend fun getUserCode(): UserCode {
        if (_userCodeDAO.getCount() == 0)
            _userCodeDAO.insert(UserCode())
        return _userCodeDAO.getFirst()
    }

    suspend fun updateUserCode(code: String) {
        val userCode = getUserCode()
        userCode.code = code
        _userCodeDAO.update(userCode)
    }
}