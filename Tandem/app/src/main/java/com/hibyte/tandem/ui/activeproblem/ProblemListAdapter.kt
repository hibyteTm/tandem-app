package com.hibyte.tandem.ui.activeproblem

import android.view.View
import androidx.annotation.LayoutRes
import com.hibyte.tandem.ui.items.ProblemItem
import com.hibyte.tandem.utils.BaseAdapter
import kotlinx.android.synthetic.main.list_item_problem.view.*

class ProblemListAdapter(@LayoutRes layoutId: Int) : BaseAdapter<ProblemItem>(layoutId) {
    override fun createViewHolder(view: View) = ProblemViewHolder(view)
}

class ProblemViewHolder(private val _view: View) : BaseAdapter.ViewHolder<ProblemItem>(_view) {
    override fun hold(
        item: ProblemItem,
        onClickListener: BaseAdapter.OnItemClickListener<ProblemItem>?
    ) {
        _view.problem_name_text_view.text = item.name
        _view.setOnClickListener {
            onClickListener?.onItemClicked(it, item)
        }
    }

}