package com.hibyte.tandem.modules

import android.content.Context
import androidx.room.Room
import com.hibyte.tandem.data.ProblemNodeLocalDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import one.space.networking.core.SpoClient
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideYourDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        ProblemNodeLocalDataBase::class.java,
        "tandem_db"
    )
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideProblemNodeDao(db: ProblemNodeLocalDataBase) = db.getProblemNodeDao()

    @Singleton
    @Provides
    fun provideUserCodeDao(db: ProblemNodeLocalDataBase) = db.getUserCodeDao()

    @Singleton
    @Provides
    fun provideSpoClient() = SpoClient.getDefault()
}