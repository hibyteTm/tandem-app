package com.hibyte.tandem.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import one.space.networking.core.annotations.SpoItemId

@Entity(tableName = "userCodes")
data class UserCode (
    @SpoItemId
    @PrimaryKey(autoGenerate = true)
    @Expose(serialize = false, deserialize = true)
    var id: Long = 0L,

    @ColumnInfo(name = "code")
    var code: String = ""
)